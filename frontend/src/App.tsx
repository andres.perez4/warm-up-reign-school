import React from 'react';
import List from './components/List/List';
import Header from './components/Header/Header';

function App() {
  return (
    <div>
      <Header />
      <List />
    </div>
  );
}

export default App;
