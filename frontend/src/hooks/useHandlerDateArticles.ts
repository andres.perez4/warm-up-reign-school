function formatAMPM(date: Date) {
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const ampm = hours >= 12 ? 'pm' : 'am';
  const minutesStr = `0${minutes}`.slice(-2);
  const strTime = `${hours}:${minutesStr} ${ampm}`;
  return strTime;
}

export default function useHandlerDateArticles(createdAt: Date): string {
  const now = new Date();
  const yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1);

  switch (true) {
    case yesterday.toLocaleDateString() === createdAt.toLocaleDateString():
      return 'Yesterday';
      break;
    case now.toLocaleDateString() === createdAt.toLocaleDateString():
      return formatAMPM(createdAt);
      break;
    default:
      return `${createdAt.toLocaleDateString('en-US', {
        month: 'short',
      })} ${createdAt.getDate()}`;
      break;
  }
}
