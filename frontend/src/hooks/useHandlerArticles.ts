import { getArticles, deleteArticle } from '../service/article.service';
import { Article } from '../interfaces/types';
import useHandlerDateArticles from './useHandlerDateArticles';

export const useHandleArticles = async () => {
  const data: Article[] = await getArticles();

  const articlesTimeAgo = data
    .sort(
      (a, b) =>
        new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
    )
    .map((article: Article) => ({
      ...article,
      created_at: useHandlerDateArticles(new Date(article.created_at)),
    }));

  return articlesTimeAgo;
};

export const useHandleDeleteArticles = async (objectID: number) => {
  const res = await deleteArticle(objectID);
  console.log(res);
  return res;
};
