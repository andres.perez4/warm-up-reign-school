import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  *{
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
      sans-serif;
    
  }

  body {
    margin: 0;
    

  }

  a {
    text-decoration: none;
  }

  h1{
    margin: 0;
    font-size: 70px;
  }

  h2{
    margin: 0;
  }
`;

export default GlobalStyle;
