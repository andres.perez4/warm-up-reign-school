import { DefaultTheme } from 'styled-components';

const theme1: DefaultTheme = {
  row: {
    bgColor: '#fff',
    border: '1px #ccc',
    hoverBgColor: '#fafafa',
    fontSize: '13pt',
  },
  title_time: {
    fontColor: '#333',
  },
  Author: {
    fontColor: '#999',
  },
};

export default theme1;
