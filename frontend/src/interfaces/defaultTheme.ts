import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    row: {
      bgColor: string;
      border: string;
      hoverBgColor: string;
      fontSize: string;
    };
    title_time: {
      fontColor: string;
    };
    Author: {
      fontColor: string;
    };
  }
}
