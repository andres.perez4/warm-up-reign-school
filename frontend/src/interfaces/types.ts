export interface Article {
  readonly objectID: number;
  readonly created_at: string;
  readonly author: string;
  readonly story_id: number;
  readonly story_title: string;
  readonly title: string;
  readonly story_url: string;
  readonly url: string;
  readonly is_deleted: Date;
}
