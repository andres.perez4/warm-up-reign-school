import React, { useEffect, useState } from 'react';
import { Article } from '../../interfaces/types';
import Row from './Row/Row';
import StyledList from './Styled/StyledList';
import Text from './Text/Text';
import {
  useHandleArticles,
  useHandleDeleteArticles,
} from '../../hooks/useHandlerArticles';
import Button from './Button/Button';

export default function List() {
  const [articles, setArticles] = useState<Article[]>([]);
  
  const filterArticles = (article: Article) => {
    if (article.is_deleted == null &&
      (article.story_url || article.url)
      && (article.story_title || article.title)
      ) {
      return true;
    }
    return false;
  }

  useEffect(() => {
    const fetchApi = async () => {
      const data = await useHandleArticles();
      setArticles(data.filter(filterArticles));
    };
    fetchApi();
  });

  return (
    <StyledList>
      {articles.map((article: Article) => (
        <div key={article.objectID}>
          <Row>
            <a
              href={article.story_url}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Text type="">{article.story_title}</Text>
              <Text type="author">-{article.author}-</Text>
              <Text type="createdAt">{article.created_at}</Text>
            </a>
            <Button onClick={() => useHandleDeleteArticles(article.objectID)}>
              Borrar
            </Button>
          </Row>
        </div>
      ))}
    </StyledList>
  );
}
