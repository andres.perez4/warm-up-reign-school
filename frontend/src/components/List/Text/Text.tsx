import React from 'react';
import StyledText from './Styled/StyledText';

export default function Text({
  children,
  type,
}: {
  children: React.ReactNode;
  type: string;
}) {
  return <StyledText type={type}>{children}</StyledText>;
}
