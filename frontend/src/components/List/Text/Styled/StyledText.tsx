import styled from 'styled-components';

interface TypeText {
  readonly type: string;
}

const StyledText = styled.div<TypeText>`
  margin-bottom: 20px;
  margin-top: 20px;
  color: ${(props) =>
    props.type === 'author'
      ? props.theme.Author.fontColor
      : props.theme.title_time.fontColor};
  padding-left: ${(props) => (props.type === 'author' ? '10px' : '0')};
`;

export default StyledText;
