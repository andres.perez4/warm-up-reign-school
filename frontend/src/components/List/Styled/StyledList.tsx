import styled from 'styled-components';

const StyledList = styled.div`
  width: 95%;
  margin: 0 auto;
  margin-top: 20px;
`;

export default StyledList;
