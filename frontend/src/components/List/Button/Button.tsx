import React from 'react';
import StyledButton from './Styled/StyledButton';

export default function Row({
  children,
  onClick,
}: {
  children: React.ReactNode;
  onClick: () => void;
}) {
  return <StyledButton onClick={onClick}>{children}</StyledButton>;
}
