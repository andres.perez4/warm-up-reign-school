import React from 'react';
import { StyledRow } from './Styled/StyledRow';

export default function Row({ children }: { children: React.ReactNode }) {
  return <StyledRow>{children}</StyledRow>;
}
