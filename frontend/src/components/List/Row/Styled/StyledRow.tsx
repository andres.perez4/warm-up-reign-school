import styled from 'styled-components';

export const StyledRow = styled.div`
  background-color: ${({ theme }) => theme.row.bgColor};
  border: ${({ theme }) => theme.row.border};
  text-color: #fff;
  font-size: ${({ theme }) => theme.row.fontSize};
  display: flex;
  align-items: center;
  flex-direction: row;
  row-gap: 10px;
  border-bottom: 2px solid ${({ theme }) => theme.Author.fontColor};

  &:hover {
    background-color: ${({ theme }) => theme.row.hoverBgColor};
  }
`;

export default StyledRow;
