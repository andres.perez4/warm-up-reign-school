import React from 'react';
import StyledHeader from './Styled/StyledHeader';

export default function Header() {
  return (
    <StyledHeader>
      <h1>HN Feed</h1>
      <h2>We {'<3'} hacker news!</h2>
    </StyledHeader>
  );
}
