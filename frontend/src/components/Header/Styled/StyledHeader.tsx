import styled from 'styled-components';

const StyledHeader = styled.header`
  color: #fff;
  background-color: ${(props) => props.theme.title_time.fontColor};
  padding: 50px;
`;

export default StyledHeader;
