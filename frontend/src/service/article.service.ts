import { Article } from '../interfaces/types';

export const getArticles = async () => {
  const success = 200;
  const res = await fetch(`${process.env.REACT_APP_API_ARTICLES}/article`);
  const data: Article[] = await res.json();

  if (res.status !== success) {
    return [];
  }

  return data;
};

export const deleteArticle = async (objectID: number) => {
  const res = await fetch(`${process.env.REACT_APP_API_ARTICLES}/${objectID}`, {
    method: 'DELETE',
  });
  return res.status;
};
