import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom, Observable } from 'rxjs';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ArticleDto } from '../article/dto/article.dto';
import { ArticleService } from '../article/article.service';
import { AxiosResponse } from 'axios';
@Injectable()
export class ApiService {
  constructor(
    private readonly httpService: HttpService,
    private readonly articleService: ArticleService,
  ) {}

  findAll(): Observable<AxiosResponse<Array<object>>> {
    return this.httpService.get(process.env.API_URL);
  }

  @Cron(CronExpression.EVERY_HOUR)
  async handleApiData() {
    Logger.log('cron job');
    const res: any = await firstValueFrom(this.findAll());
    const articles: Array<object> = res.data.hits;
    await Promise.all(
      articles.map(async (data: ArticleDto) => {
        const article = await this.articleService.getArticle(data.objectID);
        if (!article) {
          this.articleService.createArticle(data);
          Logger.log(`Article ${data.objectID} created`);
        }
      }),
    );
  }
}
