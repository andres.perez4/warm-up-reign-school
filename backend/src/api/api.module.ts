import { Module } from '@nestjs/common';
import { ApiService } from './api.service';
import { HttpModule } from '@nestjs/axios';
import { ArticleModule } from 'src/article/article.module';

@Module({
  imports: [HttpModule, ArticleModule],
  providers: [ApiService],
})
export class ApiModule {}
