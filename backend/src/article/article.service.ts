import { Injectable, Logger } from '@nestjs/common';
import { ArticleDto } from '../article/dto/article.dto';
import { Article } from '../article/schemas/article.schema';
import { ArticleRepository } from '../article/repository/article.repository';
import { InternalServerErrorException } from './handler/bbdd.exceptions';

@Injectable()
export class ArticleService {
  constructor(private readonly articleRepository: ArticleRepository) {}

  getArticles(): Promise<Article[]> {
    try {
      return this.articleRepository.getArticles();
    } catch (err) {
      Logger.error(err);
      throw new InternalServerErrorException();
    }
  }

  getArticle(objectID: number): Promise<Article> {
    try {
      return this.articleRepository.getArticle(objectID);
    } catch (err) {
      Logger.error(err);
      throw new InternalServerErrorException();
    }
  }

  createArticle(article: ArticleDto): Promise<Article> {
    try {
      return this.articleRepository.createArticle(article);
    } catch (err) {
      Logger.error(err);
      throw new InternalServerErrorException();
    }
  }

  softDeleteArticle(objectID: number): Promise<Article> {
    try {
      return this.articleRepository.softDeleteArticle(objectID);
    } catch (err) {
      Logger.error(err);
      throw new InternalServerErrorException();
    }
  }
}
