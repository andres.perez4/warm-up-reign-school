import { IsString, IsInt, IsDate } from 'class-validator';
export class ArticleDto {
  @IsInt()
  readonly objectID: number;

  @IsDate()
  readonly created_at: Date;

  @IsString()
  readonly author: string;

  @IsInt()
  readonly story_id: number;

  @IsString()
  readonly story_title: string;

  @IsString()
  readonly title: string;

  @IsString()
  readonly story_url: string;

  @IsString()
  readonly url: string;

  @IsDate()
  readonly is_deleted: Date;
}
