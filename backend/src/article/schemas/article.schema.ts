import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  objectID: number;

  @Prop()
  created_at: Date;

  @Prop()
  author: string;

  @Prop()
  story_id: number;

  @Prop()
  story_title: string;

  @Prop()
  title: string;

  @Prop()
  story_url: string;

  @Prop()
  url: string;

  @Prop({
    type: Date,
    default: null,
  })
  is_deleted: Date;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
