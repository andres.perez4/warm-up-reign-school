import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ArticleDto } from '../dto/article.dto';
import { ArticleDocument, Article } from '../schemas/article.schema';

@Injectable()
export class ArticleRepository {
  constructor(
    @InjectModel('Article')
    private readonly articleModel: Model<ArticleDocument>,
  ) {}

  getArticles(): Promise<Article[]> {
    return this.articleModel.find().exec();
  }

  getArticle(objectID: number): Promise<Article> {
    return this.articleModel.findOne({ objectID }).exec();
  }

  createArticle(article: ArticleDto): Promise<Article> {
    return new this.articleModel(article).save();
  }

  softDeleteArticle(objectID: number): Promise<Article> {
    return this.articleModel
      .findOneAndUpdate(
        { objectID: objectID },
        { is_deleted: new Date() },
        { new: true },
      )
      .exec();
  }
}
