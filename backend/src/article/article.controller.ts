import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  ParseIntPipe,
} from '@nestjs/common';

import { ArticleDto } from './dto/article.dto';

import { ArticleService } from './article.service';

@Controller('article')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  getArticles(): Promise<any> {
    return this.articleService.getArticles();
  }

  @Get('/:id')
  getArticle(@Param('id', ParseIntPipe) id: number): Promise<any> {
    return this.articleService.getArticle(id);
  }

  @Post()
  createArticle(@Body() createArticleDto: ArticleDto): Promise<any> {
    return this.articleService.createArticle(createArticleDto);
  }

  @Delete('/:id')
  deleteArticle(@Param('id', ParseIntPipe) id: number): Promise<any> {
    return this.articleService.softDeleteArticle(id);
  }
}
