import { ArticleDto } from '../../src/article/dto/article.dto';

export const ArticleDTOStub = (): ArticleDto => {
  return {
    objectID: 12345,
    created_at: new Date(),
    author: 'author',
    story_title: 'story_title',
    story_url: 'story_url',
    is_deleted: null,
  };
};
